<?php
	declare(strict_types=1);
	namespace Jca\Automodel;

	use Jca\Automodel\AutoModel;
	use Jca\Automodel\Query\MySQLiExecutor;
	use PHPUnit\Framework\TestCase;

	class AutoModelTest extends TestCase
	{
		// Request templates
		const CREATE_DATABASE = 'CREATE DATABASE `%s` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci';
		const DROP_DATABASE = 'DROP DATABASE `%s`';

		const DATABASE_NAME = 'test_db';

		protected static $db;

		/**
		 * Database schema init before all tests
		 * @method setUpBeforeClass
		 */
		public static function setUpBeforeClass() : void
		{
			// Connection init
			self::$db = new \mysqli($GLOBALS['DB_HOST'], "root", "71w%U5^zZS7c");
			

			// Database creation
			$sql = sprintf(self::CREATE_DATABASE, self::DATABASE_NAME);
			self::$db->query($sql);
			self::$db->select_db(self::DATABASE_NAME);

			// Schema init
			$sql = "CREATE TABLE dummy
			(
				id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
				label VARCHAR(200)
			)";
			self::$db->query($sql);

			AutoModel::setExecutor(new MySQLiExecutor($GLOBALS['DB_HOST'], "root", "71w%U5^zZS7c", self::DATABASE_NAME));
		}

		/**
		 * Database delete after all tests
		 * @method tearDownAfterClass
		 */
		public static function tearDownAfterClass() : void
		{
			// Database delete
			$sql = sprintf(self::DROP_DATABASE, self::DATABASE_NAME);
			self::$db->query($sql);

			self::$db->close();
			self::$db = null;
		}

		/**
		 * Before each test
		 * @method setUp
		 */
		protected function setUp() : void
		{

		}

		/**
		 * After each test
		 * @method tearDown
		 */
		protected function tearDown() : void
		{

		}

		/**
		 * Columns mapping test
		 * @method testFieldsMapping
		 */
		public function testFieldsMapping(): void
		{
			$model = new AutoModel("dummy");
			$this->assertTrue(property_exists($model, 'id'));
			$this->assertTrue(property_exists($model, 'label'));
		}

		/**
		 * Rows search test
		 * @method testReccordSearch
		 */
		public function testReccordSearch(): void
		{
			// Single result
			$sql = "INSERT INTO dummy VALUES(1, 'test')";
			self::$db->query($sql);
			$models = AutoModel::findDummyById(1)[0];
			$this->assertInstanceOf('Jca\Automodel\AutoModel', $models);

			// Multiple results
			$sql = "INSERT INTO dummy VALUES(2, 'test')";
			self::$db->query($sql);
			$models = AutoModel::findDummyByLabel('test');
			$this->assertEquals(2, count($models));
			$this->assertEquals('test', $models[0]->label);
		}

		public function testReccordCreate(): void
		{
			$model = new AutoModel("dummy");
			$model->label = 'test';
			$model->save();

			$model = AutoModel::findDummyById(1)[0];
			$this->assertInstanceOf('Jca\Automodel\AutoModel', $model);
		}
	}

?>
