# Automodel

Automodel is a PHP library for mapping databases structure in classes, and use them without having to deal with SQL requests.

[![pipeline status](https://gitlab.com/jcaillabet/automodel/badges/test_ci/pipeline.svg)](https://gitlab.com/jcaillabet/automodel/-/commits/test_ci)
[![coverage report](https://gitlab.com/jcaillabet/automodel/badges/test_ci/coverage.svg)](https://gitlab.com/jcaillabet/automodel/-/commits/test_ci)

# Testing

Tests can be runned by simply launching the docker-compose configuration:

<code>docker-compose up --build</code>

# Add to composer

This project can be added as a composer dependency :

<code>
	composer config repositories.58018364 composer https://gitlab.com/api/v4/group/58018364/-/packages/composer/packages.json
	composer require jca/automodel:0.0.2
</code>
