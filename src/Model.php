<?php

namespace Jca\Automodel;

use Jca\Automodel\Validation\Validation;

/**
 * Model description class
 *
 * @author Julien Caillabet <jcaillabet@gmail.com>
 * @access public
 */
abstract class Model extends \stdClass
{
	/**
	 * Form labels
	 * @var array
	 */
	protected $labels = [];

	/**
	 * Form validation rules
	 * @var array
	 */
	protected $rules = [];

	protected $errors = [];

	protected $fields = [];

	protected $types = [];

	protected $persist = [];

	public function __construct()
	{
		$this->reflexion = new \ReflectionClass($this);
	}

	/**
	 * Returns a label
	 * @method getLabel
	 * @param  string  $field  Field to get label
	 * @return string  Label value
	 */
	public function getLabel($field)
	{
		if(!array_key_exists($field, $this->labels))
			return $field;
		return $this->labels[$field];
	}

	/**
	 * Returns rules
	 * @method getRules
	 * @param  string  $field  Field to get rules
	 * @return array   Rules list
	 */
	/*public function getRules($field)
	{
		$rules = [];

		$attributes = $this->getAttributes($field);
		
		//foreach()


		return $rules;
	}*/

	/**
	 * Returns rules
	 * @method getRules
	 * @param  string  $field  Field to get rules
	 * @return array   Rules list
	 */
	public function getValidation($field)
	{
		// Method
		$properties = $this->reflexion->getProperties();
		$property = current(array_filter($properties, function($item) use ($field)
		{
			return $item->name == $field;
		}));

		if(!$property)
			return [];

		// Attribute
		$attributes = $property->getAttributes();
		return array_filter($attributes, function($item)
		{
			return get_parent_class($item->newInstance()) == Validation::class;
		});
	}

	public function getRules($field)
	{
		$validations = $this->getValidation($field);

		$rules = [];
		foreach($validations as $validation)
		{
			$instance = $validation->newInstance();
			$rules[strtolower((new \ReflectionClass($instance))->getShortName())] = $validation->newInstance()->getValue();
		}
			
		return json_encode($rules);
	}

	private function getAttribute($class, $field)
	{
		// Method
		$properties = $this->reflexion->getProperties();
		$property = current(array_filter($properties, function($item) use ($field)
		{
			return $item->name == $field;
		}));

		// Attribute
		$attributes = $property->getAttributes();
		$attribute = current(array_filter($attributes, function($item) use($class)
		{
			return $item->getName() == $class;
		}));

		return $attribute;
	}

	public function getErrors($field)
	{
		if(array_key_exists($field, $this->errors))
			return json_encode($this->errors[$field]);
		return json_encode([]);
	}

	public function getType($field)
	{
		return $this->types[$field];
	}

	/**
	 * Fill model with data from a request result
	 * @method fill
	 * @param  array $data Data to import
	 */
	public function fill($data)
	{
		foreach ($data as $key => $value)
			$this->$key = $value;
	}

	public function validate($data)
	{
		if(empty($data))
			return false;

		foreach($data as $field => $value)
		{
			$validations = $this->getValidation($field);
			foreach($validations as $validation)
			{
				$instance = $validation->newInstance();

				if(!$instance->isValid($value))
					$this->errors[$field][strtolower((new \ReflectionClass($instance))->getShortName())] = $instance->getErrorMessage();

				/*$this->errors[$field] = [];
				foreach($rules as $name => $params)
				{
					if($name == 'equals')
					{
						$compare_field = $params[0];
						if($this->$field != $this->$compare_field)
						{
							$this->errors[$field][$name] = 'Les deux mots de passe doivent être égaux';
						}
					}
					elseif($name == 'length')
					{
						$length = $params[0];
						if(strlen($this->$field) < $length)
						{
							$this->errors[$field][$name] = "Doit faire au moins $length caractères";
						}
					}
					elseif($name == 'captcha')
					{
						if(empty($this->$field) || $name . '_code')
						{
							$this->errors[$field][$name] = 'Captcha incorrect';
						}
					}
				}*/
			}
		}

		return empty($this->errors);
	}

	/*public function validate($form_data)
	{
		foreach($this->rules as $rule => $fields)
		{
			foreach($form_data as $field => $value)
			{
				if(in_array($field, $fields))
				{
					if( $rule == 'captcha' )
					{
						if( array_key_exists($field . '_code', $_SESSION) && $_SESSION[$field . '_code'] != $value )
						{
							$this->errors[] = $field;
							return false;
						}
					}
				}
			}
		}
		return count($this->errors) == 0;
	}*/
}

?>
