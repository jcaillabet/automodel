<?php
	namespace Jca\Automodel\Query;
	
	//use Jca\Automodel\Processing\MethodProcessor;
    class MySQLiExecutor extends QueryExecutor
    {
		private $db;

		public function __construct()
		{
			$get_arguments       = func_get_args();
        	$number_of_arguments = func_num_args();

			$method_name = '__construct' . $number_of_arguments;
			if (method_exists($this, $method_name))
				call_user_func_array(array($this, $method_name), $get_arguments);
		}

		private function __construct3($user, $password, $db_name) 
		{
			self::__construct4("db", $user, $password, $db_name);
		}

		private function __construct4($host, $user, $password, $db_name)
		{
			$this->db = new \mysqli($host, $user, $password, $db_name);
		}

        public function runSQL($sql)
		{
			return $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);
		}

		public function describeTable($table_name)
		{
			return $this->runSQL(sprintf(self::LIST_TABLE_COLUMNS, $table_name));
		}

		public function getForeignTable($field, $table_name)
		{
			$result = $this->runSQL(sprintf(self::GET_FOREIGN_KEY_TABLE, $field, $table_name));	
			return current($result);
		}

		public function getExternalTables($table_name, $field)
		{
			return $this->runSQL(sprintf(self::GET_FOREIGN_KEY_SOURCES, $table_name, $field));
		}

		public function getRows($fields, $table_name, $where = '')
		{
			return $this->runSQL(sprintf(self::SELECT_ROWS, implode(', ', $fields), $table_name) . $where);
		}

		public function execute_query($sql, $values)
		{
			return $this->db->execute_query($sql, $values);
		}

		public function query($sql)
		{
			return $this->db->query($sql);
		}

		public function multi_query($sql)
		{
			return $this->db->multi_query($sql);
		}

		public function prepare($sql)
		{
			return $this->db->prepare($sql);
		}

		public function getLastID()
		{
			return $this->db->insert_id;
		}

		public function next_result()
		{
			return $this->db->next_result();
		}
    }
?>