<?php
	namespace Jca\Automodel\Query;
	
	//use Jca\Automodel\Processing\MethodProcessor;
    abstract class QueryExecutor
    {
        protected const LIST_TABLE_COLUMNS = 'SHOW COLUMNS FROM %s';
		protected const LIST_TABLE_PRIMARY_KEYS = "SHOW COLUMNS FROM %s WHERE `Key` = 'PRI'";
		protected const SELECT_ROWS = 'SELECT %s FROM %s';
		protected const INSERT_ROW = 'INSERT INTO %s(%s) VALUES(%s)';
		protected const UPDATE_ROW = 'UPDATE %s SET %s WHERE id=%s';
		protected const DELETE_ROW = 'DELETE FROM %s WHERE id=%s';
		protected const GET_FOREIGN_KEY_TABLE = "SELECT REFERENCED_TABLE_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE COLUMN_NAME = '%s' AND TABLE_NAME = '%s'";
		protected const GET_FOREIGN_KEY_SOURCES = "SELECT TABLE_NAME,COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_NAME = '%s' AND REFERENCED_COLUMN_NAME = '%s'";

        public abstract function __construct();

        public abstract function runSQL($sql);

		public abstract function describeTable($table_name);

		public abstract function getForeignTable($field, $table_name);

		public abstract function getExternalTables($table_name, $field);

		public abstract function getRows($fields, $table_name, $where = '');

		public abstract function execute_query($sql, $values);

		public abstract function query($sql);

		public abstract function multi_query($sql);

		public abstract function prepare($sql);

		public abstract function getLastID();

		public abstract function next_result();
    }
?>