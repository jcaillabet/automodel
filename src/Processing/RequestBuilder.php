<?php

namespace Jca\Automodel\Processing;

/**
 * SQL request builder
 */
class RequestBuilder
{
	/**
	 * Table name
	 * @var string
	 */
	private $table = "";

	/**
	 * Where conditions
	 * @var array
	 */
	private $conditions = [];

	/**
	 * Request count fields
	 * @var array
	 */
	private $counts = [];

	/**
	 * Request order fields
	 * @var array
	 */
	private $orders = [];

	/**
	 * Request limit
	 * @var int
	 */
	private $limit;

	/**
	 * Request offset
	 * @var int
	 */
	private $offset;

	private function __construct(){}

	public static function build()
	{
		return new RequestBuilder();
	}

	/**
	 * Set request table
	 * @param  string $table  Table name
	 */
	public function withTable($table)
	{
		$this->table = $table;
		return $this;
	}

	public function getTable()
	{
		return $this->table;
	}

	/**
	 * Add a 'where' condition
	 * @param  string  $condition  'where' condition string
	 */
	public function withCondition($condition)
	{
		array_push($this->conditions, $condition);
		return $this;
	}

	/**
	 * Add a 'count' field
	 * @param  string  $count  'key' to count
	 */
	public function withCount($count)
	{
		array_push($this->counts, $count);
		return $this;
	}

	/**
	 * Add a 'order' field
	 * @param  string  $count  'key' to count
	 */
	public function withOrder($order)
	{
		// TODO remove current
		array_push($this->orders, current(array_map(function($key, $value)
		{
			return "$key $value";
		}, array_keys($order), array_values($order))));
		return $this;
	}

	/**
	 * Set the request limit
	 * @method withLimit
	 * @param  int  $limit  Limit value
	 */
	public function withLimit($limit)
	{
		$this->limit = $limit;
	}

	/**
	 * Set the request offset
	 * @method withOffset
	 * @param  int  $offset  Offset value
	 */
	public function withOffset($offset)
	{
		$this->offset = $offset;
	}

	/**
	 * Return the SQL string corresponding to the builder
	 * @return string SQL request
	 */
	public function asSQL()
	{
		$params = [];

		$sql = "select $this->table.*";
		if(!empty($this->counts))
			$sql .= ', ' . implode(', ', array_map(function($count){return "count($count.{$this->table}_id) as {$count}_count";}, $this->counts));
		$sql .= " from $this->table";
		if(!empty($this->counts))
			$sql .= ', ' . implode(', ', $this->counts);

		$values = null;
		if(!empty($this->conditions))
		{
			$sql .= " where ";

			$sql .= implode(' and ', array_map(function($item)
			{
				return $item['literal'];
			}, $this->conditions));

			$values = array_map(function($item)
			{
				return $item['value'];
			}, array_filter($this->conditions, function($item)
			{
				return $item['value'];
			}));
		}
		if(!empty($this->counts))
			$sql .= " and ". implode('and ', array_map(function($count){return "$count.{$this->table}_id={$this->table}.id";}, $this->counts));
		if(!empty($this->counts))
			$sql .= " group by {$this->table}.id";
		if(!empty($this->orders))
			$sql .= " order by " . implode(', ', $this->orders);
		if($this->limit != null)
			$sql .= " limit " . $this->limit;
		if($this->offset != null)
			$sql .= " offset " . $this->offset;

		return ['sql' => $sql,
				'values' => $values];
	}
}

?>
