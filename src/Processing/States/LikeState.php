<?php
	namespace Jca\Automodel\Processing\States;

	use Jca\Automodel\Processing\MethodState;

	/**
	 * Condition state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class LikeState extends MethodState
	{
		/*public function __construct($buffer = [])
		{

		}*/

		public function process()
		{
			$word =  $this->context->unstack();

			if(in_array($word, [null, "Limit"]))
			{
				$value = $context->unstackArgument();
				if(gettype($value) == 'string'){
					$type = 's';
				}
				if(gettype($value) == 'integer'){
					$type = 'i';
				}
				if(gettype($value) == 'boolean')
					$type = 'i';
				$context->getOutput()->withCondition([
					'literal' => 'lower(' . $this->buffer["field"] . ') like ?',
					'type' => $type,
					'value' => '%' . $value . '%'
				]);
			}

			if($word == null)
			{
				$context->setState(null);
			}
			else if($word == "Limit")
			{
				$context->setState(new LimitState());
			}
		}
	}
?>
