<?php
	namespace Jca\Automodel\Processing\States;

	use Jca\Automodel\Processing\MethodState;

	/**
	 * Find request state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class TableState extends MethodState
	{
		public function process()
		{
			$word =  $this->context->unstack();

			if($word == "By")
			{
				$class_path = $context->getCallingClass();
				
				$class_path_array = explode('\\', $class_path);
				$class_name = end($class_path_array);

				$words = preg_split('@((?<=.)(?=[[:upper:]][[:lower:]])|(?<=[[:lower:]])(?=[[:upper:]]))@',$class_name);
				$words = array_map(function($word)
				{
					return strtolower($word);
				}, $words);

				$table_name = implode('_', $words);

				$context->getOutput()->withTable($table_name);
				$context->setState(new ConditionState());
			}
			else
			{
				$context->getOutput()->withTable(strtolower($word));
				$context->setState($this->getNextState($word));
			}

			/*if($word == "And")
			{
				$context->setState(new ConditionState());
			}
			else if($word == "By")
			{
				$context->setState(new ConditionState());
			}
			else
			{
				$context->getOutput()->withTable(strtolower($word));
				$context->setState($this->getNextState($word));
			}*/
		}
	}
?>
