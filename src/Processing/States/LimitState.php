<?php
	namespace Jca\Automodel\Processing\States;

	use Jca\Automodel\Processing\MethodState;

	/**
	 * Limit state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class LimitState extends MethodState
	{
		public function process()
		{
			$word =  $this->context->unstack();

			// Request limit
			$value = $context->shiftArgument();
			$context->getOutput()->withLimit($value);

			if($word == 'Offset')
			{
				$context->setState(new OffsetState());
			}
			if($word == null)
			{
				$context->setState(null);
			}
		}
	}
?>
