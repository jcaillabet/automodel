<?php
	namespace Jca\Automodel\Processing\States;

	use Jca\Automodel\Processing\MethodState;

	/**
	 * Limit state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class CountState extends MethodState
	{
		public function process()
		{
			$input = $context->getInput();
			$arg = array_shift($input['arguments']);
			$context->setInput($input);

			// Request limit
			$context->getOutput()->withCount($arg);

			// Next state
			$input = $context->getInput();
			$word = array_shift($input['words']);
			$context->setInput($input);

			$context->setState($this->getNextState($word));
		}
	}
?>
