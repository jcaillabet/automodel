<?php
	namespace Jca\Automodel\Processing\States;

	use Jca\Automodel\Processing\MethodState;

	/**
	 * Find request state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class FindState extends MethodState
	{
		public function process()
		{
			$word =  $this->context->unstack();

			if(in_array($word, ["Limit", "Order", "By"]))
			{
				if($this->context->getCallingClass() !=  'Jca\Automodel\AutoModel' && empty($this->context->getBuilder()->getTable()) )
				{
					$explode = explode('\\', $this->context->getCallingClass());
					$class_name = end($explode);
					$words = preg_split('@((?<=.)(?=[[:upper:]][[:lower:]])|(?<=[[:lower:]])(?=[[:upper:]]))@',$class_name);
					$words = array_map(function($word)
					{
						return strtolower($word);
					}, $words);
					$table_name = implode('_', $words);
					$this->context->getBuilder()->withTable(strtolower($table_name));
				}
			}

			if($word == "Limit")
			{
				$this->context->setState(new LimitState($this->context));
			}
			else if($word == "Order")
			{
				$this->context->setState(new OrderState($this->context));
			}
			else if($word == "By")
			{
				$this->context->setState(new WhereState($this->context));
			}
			else if($word == null)
			{
				$this->context->setState(null);
			}
			else
			{
				$this->context->getBuilder()->withTable(strtolower($word));
				$this->context->setState(new FindState($this->context));
			}
		}
	}
?>
