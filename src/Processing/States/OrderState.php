<?php
	namespace Jca\Automodel\Processing\States;

	use Jca\Automodel\Processing\MethodState;

	/**
	 * Limit state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class OrderState extends MethodState
	{
		public function process()
		{
			$word =  $this->context->unstack();

			$value = $this->context->unstackArgument();
			$this->context->getBuilder()->withOrder($value);

			if($word == null)
			{
				$this->context->setState(null);
			}
			else if($word == "Limit")
			{
				$this->context->setState(new LimitState());
			}
		}
	}
?>
