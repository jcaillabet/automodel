<?php
	namespace Jca\Automodel\Processing\States;

	use Jca\Automodel\Processing\MethodState;

	/**
	 * Condition state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class NullState extends MethodState
	{
		/*public function __construct($buffer = [])
		{

		}*/

		public function process()
		{
			$word =  $this->context->unstack();

			if(in_array($word, [null]))
			{
				$value = $context->shiftArgument();
				$context->getOutput()->withCondition([
					'literal' => $this->buffer["field"] . ' IS NULL',
					'type' => 'null',
					'value' => $value
				]);
			}

			if($word == null)
			{
				$context->setState(null);
			}
		}
	}
?>
