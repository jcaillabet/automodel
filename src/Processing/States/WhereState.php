<?php
	namespace Jca\Automodel\Processing\States;

	use Jca\Automodel\Processing\MethodState;

	/**
	 * Condition state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class WhereState extends MethodState
	{
		/*public function __construct($buffer = [])
		{

		}*/

		public function process()
		{
			$word =  $this->context->unstack();

			if(in_array($word, [null, "And", "Limit", "Order"]))
			{
				$value = $this->context->unstackArgument();
				$this->context->getBuilder()->withCondition([
					'literal' => $this->buffer["field"] . ' = ?',
					'type' => 'value',
					'value' => $value
				]);
			}

			if($word == null)
			{
				$this->context->setState(null);
			}
			else if($word == "Order")
			{
				$this->context->setState(new OrderState($this->context));
			} 
			else if($word == "Limit")
			{
				$this->context->setState(new LimitState($this->context));
			} 
			else if($word == "Like")
			{
				$this->context->setState(new LikeState($this->context));
			} 
			else if($word == "Null")
			{
				$this->context->setState(new NullState($this->context));
			} 
			else if($word == "And")
			{
				$this->context->setState(new WhereState($this->context));
			} 
			else
			{
				$this->context->setState(new WhereState($this->context, ['field' => strtolower($word)]));
			}

			//field name
			/*$input = $this->context->getInput();
			$field = array_shift($input['words']);
			$this->context->setInput($input);
			$field = strtolower($field);
			$condition = $field;

			// Operator
			$input = $this->context->getInput();
			$operator = array_shift($input['words']);
			$this->context->setInput($input);

			if($operator == "Like")
			{
				// Like value
				$term = array_shift($input['arguments']);
				$this->context->setInput($input);

				$condition .= " LIKE '%" . $term . "%'";

				// Condition string
				$this->context->getOutput()->withCondition(html_entity_decode($condition));

				// Next state
				$input = $this->context->getInput();
				$word = array_shift($input['words']);
				$this->context->setInput($input);

				$this->context->setState($this->getNextState($word));
			}
			else if($operator == "Not")
			{
				// Sub operator
				$input = $this->context->getInput();
				$operator = array_shift($input['words']);
				$this->context->setInput($input);
				if($operator == "Null")
					$condition .= " IS NOT NULL";

				// Condition string
				$this->context->getOutput()->withCondition(html_entity_decode($condition));

				// Next state
				$input = $this->context->getInput();
				$word = array_shift($input['words']);
				$this->context->setInput($input);

				$this->context->setState($this->getNextState($word));
			}
			else
			{
				//$input = $this->context->getInput();
				$arg = array_shift($input['arguments']);
				$this->context->setInput($input);

				$condition .= ' = ' . $arg;

				// Condition string
				$this->context->getOutput()->withCondition(html_entity_decode($condition));

				// Next state

				$this->context->setState($this->getNextState($operator));
			}*/
		}
	}
?>
