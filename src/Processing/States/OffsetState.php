<?php
	namespace Jca\Automodel\Processing\States;

	use Jca\Automodel\Processing\MethodState;

	/**
	 * Offset state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class OffsetState extends MethodState
	{
		public function process()
		{
			$word = $this->context->unstack();

			// Request limit
			$value = $context->shiftArgument();
			$context->getOutput()->withOffset($value);

			if($word == null)
			{
				$context->setState(null);
			}
		}
	}
?>
