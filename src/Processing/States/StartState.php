<?php
	namespace Jca\Automodel\Processing\States;

	use Jca\Automodel\Processing\MethodState;
	use Jca\Engine\Exceptions\HTTPException;

	/**
	 * Find request state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class StartState extends MethodState
	{
		public function process()
		{
			$word =  $this->context->unstack();

			if($word == "find")
				$this->context->setState(new FindState($this->context));
			else 
				throw new \Exception("");
		}
	}
?>
