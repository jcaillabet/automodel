<?php
	namespace Jca\Automodel\Processing;

	use Jca\State\ConcreteState;

	use Jca\Automodel\Processing\States\CountState;
	use Jca\Automodel\Processing\States\LimitState;
	use Jca\Automodel\Processing\States\OffsetState;
	use Jca\Automodel\Processing\States\OrderState;
	use Jca\Automodel\Processing\States\TableState;

	/**
	 * MethodProcessor machine state
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	abstract class MethodState extends ConcreteState
	{
		protected $buffer;

		public function __construct($context, $buffer = [])
		{
			parent::__construct($context);

			$this->buffer = $buffer;
		}

		protected function getNextState($word)
		{
			if($word == "find")
				return new TableState();
			if($word == "Count")
				return new CountState();
			if($word == "Limit")
				return new LimitState();
			if($word == "Offset")
				return new OffsetState();
			if($word == "Order")
				return new OrderState();
			return null;
		}

		public function getBuilder(){ return $this->builder; }
	}
?>
