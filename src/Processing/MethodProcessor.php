<?php
	namespace Jca\Automodel\Processing;

	use Jca\Automodel\Processing\States\StartState;
	use Jca\State\Machines\Processor;

	/**
	 * State machine for missing request method
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class MethodProcessor extends Processor
	{
		private $calling_class;

		private $builder;

		/**
		 * MethodProcessor constructor
		 *
		 * @param  string $name       Method name
		 * @param  array  $arguments  Method arguments
		 */
		public function __construct($name, $arguments)
		{
			parent::__construct(new StartState($this), ['name' => $name, 
							'arguments' => $arguments]);

			$this->builder = RequestBuilder::build();
		}

		public function init($input): array
		{
			return ['words' => preg_split('/(?=[A-Z])/', $input['name']), 
				'arguments' => $input['arguments']];
		}

		public function setCallingClass($calling_class)
		{
			$this->calling_class = $calling_class;
		}

		public function getCallingClass()
		{
			return $this->calling_class;
		}

		public function unstack()
        {
			return array_shift($this->stack['words']);
        }

		public function unstackArgument()
        {
			return array_shift($this->stack['arguments']);
        }

		public function getBuilder(){ return $this->builder; }
	}
?>
