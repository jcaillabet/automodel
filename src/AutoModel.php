<?php
	namespace Jca\Automodel;

	use Jca\Automodel\Cache;
	use Jca\Automodel\Processing\MethodProcessor;
	use Jca\Automodel\Utils\NameUtils;

	/**
	 * Automodel is a class to map database tables without dealing with SQL
	 * requests
	 *
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class AutoModel extends Model
	{
		// Request templates
		private const SELECT_ROWS = 'SELECT %s FROM %s';
		private const INSERT_ROW = 'INSERT INTO %s(%s) VALUES(%s)';
		private const UPDATE_ROW = 'UPDATE %s SET %s WHERE id = %s';
		private const DELETE_ROW = 'DELETE FROM %s WHERE id = %s';

		private static $caches = [];

		/**
		 * Database access
		 * @var PDO
		 */
		private static $db;
		private static $executor;

		/**
		 * Automodel constructor
		 * @method __construct
		 * @param  string  $table_name  Name of mapped table
		 */
		public function __construct($table_name = null)
		{
			parent::__construct();

			// Current class init
			if(empty($table_name))
				$table_name = NameUtils::classNameToTableName($this->reflexion->getShortName());
			$cache = self::getCache($table_name);
			if(empty($cache))
			{
				$cache = new Cache($table_name);
				self::$caches[] = $cache;
			}
			$this->main_cache = $cache;
			$this->init($cache);

			// Parent class init
			$parent_class = $this->reflexion->getParentClass();
			$table_name = NameUtils::classNameToTableName($parent_class->getShortName());
			
			$test = $parent_class->getName();
			if(!in_array($parent_class->getName() ,['Jca\Automodel\AutoModel', 'Jca\Automodel\Model']))
			{
				$cache = self::getCache($table_name);
				if(empty($cache))
				{
					$cache = new Cache($table_name);
					self::$caches[] = $cache;
					$this->init($cache);
				}
			}
		}

		protected static function getCache($table_name)
		{
			return current(array_filter(self::$caches, function($item) use ($table_name)
			{
				$test3 = $item->table_name === $table_name;
				return $item->table_name === $table_name;
			}));
		}

        protected function init($cache)
		{
			foreach($cache->getColumns() as $field => $row)
			{
				$default = $row['Default'];
				$name = $field;

				if($row['Default'] != null && $row['Key'] == 'MUL')
				{
					$name = $row['REFERENCED_TABLE_NAME'];
					$default = $row['relation'];
				}

				// Field name
				$this->$name = $default;

				// Rules
				if($row['Null'] == 'NO')
					$this->rules[$name]['required'] = [];
			}

			foreach($cache->getExternalTables() as $externable_table => $data)
			{
				$array_name = $externable_table . 's';
				$this->$array_name = [];
			}
		}

		public static function build($table_name, $row = [])
		{
			$class_path = NameUtils::tableNameToClassPath($table_name);

			if(class_exists($class_path))
				return new $class_path();
			return new AutoModel($table_name);
		}

		/**
		 * Get the model table name
		 * @return string Model table name
		 */
		public function getTableName()
		{
			return $this->table_name;
		}

		/**
		 * Set the db connector
		 * @param  string $db	Connector to set
		 */
		static function setDb($db) {
			self::$db = $db;
		}
		static function setExecutor($executor) {
			self::$executor = $executor;
		}

		static function getExecutor() {
			return self::$executor;
		}

		public function getColumns()
		{
			$class_name = substr(strrchr(get_class($this), '\\'), 1);
			$table_name = NameUtils::classNameToTableName($class_name);
			$columns = self::getCache($table_name)->getColumns();

			$parent = get_parent_class($this);
			if($parent != AutoModel::class) {
				$class_name = substr(strrchr($parent, '\\'), 1);
				$table_name = NameUtils::classNameToTableName($class_name);
				$columns = array_merge($columns, self::getCache($table_name)->getColumns())	;
			}

			return $columns;
		}

		public function getExternalTables()
		{
			$class_name = substr(strrchr(get_class($this), '\\'), 1);
			$table_name = NameUtils::classNameToTableName($class_name);
			$tables = self::getCache($table_name)->getExternalTables();

			$parent = get_parent_class($this);
			if($parent != AutoModel::class) {
				$class_name = substr(strrchr($parent, '\\'), 1);
				$table_name = NameUtils::classNameToTableName($class_name);
				$tables = array_merge($tables, self::getCache($table_name)->getExternalTables())	;
			}

			return $tables;
		}

		/*protected static function initCache($table_name)
		{
			if(array_key_exists($table_name, self::$cache))
				return;

			self::$cache[$table_name]['columns'] = [];
			self::$cache[$table_name]['external_tables'] = [];
			
			$columns = self::$executor->describeTable($table_name);

			foreach($columns as $column)
			{
				// Column data
				$field = $column['Field'];
				unset($column['Field']);
				self::$cache[$table_name]['columns'][$field] = $column;
			
				// Foreign key
				if($column['Key'] == 'MUL')
				{
					$foreign_table = self::$executor->getForeignTable($field, $table_name);
					self::$cache[$table_name]['columns'][$field]['REFERENCED_TABLE_NAME'] = $foreign_table['REFERENCED_TABLE_NAME'];

					// Default value
					if(!empty($column['Default']))
					{
						$methodName = 'find' . ucfirst($foreign_table['REFERENCED_TABLE_NAME']) . 'ById';
						$relation = self::$methodName($column['Default'])[0];
						self::$cache[$table_name]['columns'][$field]['relation'] = $relation;
					}
				}

				// External tables
				$external_tables = self::$executor->getExternalTables($table_name, $field);
				foreach($external_tables as $external_table)
				{
					$external_cols = self::$executor->describeTable($external_table['TABLE_NAME']);
					
					$int = current(array_filter($external_cols, fn($item) => $item['Field'] == $external_table['COLUMN_NAME'] && $item['Key'] == 'MUL'));
					$ext = current(array_filter($external_cols, fn($item) => $item['Field'] != $external_table['COLUMN_NAME'] && $item['Key'] == 'MUL'));
					
					if( $int && $ext )
						self::$cache[$table_name]['external_tables'][$external_table['TABLE_NAME']] = ['internal_column' => $int['Field'], 'external_column' => $ext['Field']];
				}
			}
		}*/

		private function getFields($cache)
		{
			return array_keys(array_filter($cache->getColumns(), function($item, $key)
			{
				// Mandatory fields
				if($item['Null'] == "NO" && $item['Extra'] != 'auto_increment')
					return true;
				// Optional fields
				if(isset($this->{$key}) && $this->{$key} != null && $item['Extra'] != 'auto_increment')
					return true;
				return false;
			}, ARRAY_FILTER_USE_BOTH));
		}

		private function getValues($cache, $fields)//$cache)
		{
			return array_map(function ($item) use ($cache)
			{
				if($cache->getColumns()[$item]['Key'] == 'MUL')
				{	
					$field = $cache->getColumns()[$item]['REFERENCED_TABLE_NAME'];
					if(property_exists($this, $field))
						return($this->$field)->id;
					return null;
					
				}
				else
					return $this->$item;
			}, $fields);
		}

		private function buildInsertQuery($table_name, $fields)
		{
			$tokens = array_map(function ($item)
			{
				return '?';
			}, $fields);

			return $sql = sprintf(self::INSERT_ROW, $table_name, implode(", ", $fields), implode(", ", $tokens));
		}

		private function buildUpdateQuery($table_name, $fields, $values)
		{
			$sets = array_map(function($key, $value)
			{
				return $key . ' = ?';// . $value;
			}, $fields, $values);


			$id = $this->id;
			if($table_name != NameUtils::classNameToTableName($this->reflexion->getShortName()))
			{
				$field = $table_name . "_id";
				$id = $this->$field;
			}

			return sprintf(self::UPDATE_ROW,
				$table_name,
				implode(", ", $sets),
				$id);
		}

		private function buildSaveQuery($table_name, $fields, $values, $force_insert)
		{
			// SQL request
			// TODO generic ID
			if( !empty($this->id) && !$force_insert) 
			{
				$sets = array_map(function($key, $value)
				{
					return $key . ' = ?';// . $value;
				}, $fields, $values);

 				return $this->buildUpdateQuery($table_name, $fields, $values);
			}
			else {
				$sets = array_map(function($key, $value)
				{
					return $key . ' = ?';// . $value;
				}, $fields, $values);

				return $this->buildInsertQuery($table_name, $fields);
			}
		}

		// Create
		/**
		 * Save object in base
		 * @method save
		 * @return bool save has succeded
		 */
		public function save($force_insert = false)
		{
			// Parent class save
			$parent_class = $this->reflexion->getParentClass();
			if($parent_class->getName() != ' Jca\Automodel\Automodel')
			{
				$parent_table_name = NameUtils::classNameToTableName($parent_class->getShortName());
				$cache = $this->getCache($parent_table_name);
				
				$fields = $this->getFields($cache);
				$values = $this->getValues($cache, $fields);
				
				$sql = $this->buildSaveQuery($parent_table_name, $fields, $values, $force_insert);
	
				try
				{
					$result = self::$executor->execute_query($sql, $values);

					if(empty($this->id))
						$parent_id = self::$executor->getLastID();
					else
					{
						$field = $parent_table_name . "_id";
						$parent_id = $this->$field;
					}
						

				}
				catch (\mysqli_sql_exception $e)
				{
					return false;
				}
			}

			$table_name = NameUtils::classNameToTableName($this->reflexion->getShortName());
			$cache = $this->getCache($table_name);
			
			$fields = $this->getFields($cache);
			$values = $this->getValues($cache, $fields);

			if($parent_class->getName() != ' Jca\Automodel\Automodel')
			{
				$field = current(array_keys(array_filter($cache->getColumns(), function($item) use($parent_table_name)
				{
					if(!array_key_exists('REFERENCED_TABLE_NAME', $item))
						return false;
					return $item['REFERENCED_TABLE_NAME'] == $parent_table_name;
				})));
				$fields[] = $field;
				$values[] = $parent_id;
			}
			
			$sql = $this->buildSaveQuery($table_name, $fields, $values, $force_insert);

			try
			{
				$result = self::$executor->execute_query($sql, $values);

				if(empty($this->id))
					$this->id = self::$executor->getLastID();

				// External tables
				$external_tables = self::getCache($table_name)->getExternalTables();
				foreach($external_tables as $table => $columns)
				{
					$field = "{$table}s";
					$values = $this->$field;

					$sql = sprintf('DELETE FROM %s WHERE %s = ?', $table, $columns["internal_column"]);
					self::$executor->execute_query($sql, [$this->id]);

					$fields = implode(', ', array_values($columns));
					$values = [];
					foreach($this->$field as $value)
					{
						$sql = sprintf(self::INSERT_ROW, $table, $fields, "?, ?");
						self::$executor->execute_query($sql, [$this->id, $value]);
					}
				}

				return true;
			}
			catch (\mysqli_sql_exception $e)
			{
				return false;
			}
		}

		/**
		 * Fill model with data from a request result
		 * @method fill
		 * @param  array $data Data to import
		 */
		public function fill($data)
		{
			foreach ($data as $key => $value)
				$this->$key = $value;
		}

		public function getSubModel($field)
		{
			$table_name = NameUtils::classNameToTableName($this->reflexion->getShortName());

			if(!$this->main_cache->getColumns()[$field]['Key'] == 'MUL')
				return false;
			if($this->$field == null)
				return null;

			$foreign_table = $this->main_cache->getColumns()[$field]['REFERENCED_TABLE_NAME'];
			$sql = sprintf(self::SELECT_ROWS, '*', $foreign_table) . ' where id = ?';
			$res = self::$executor->execute_query($sql, [$this->$field]);
			if($row = $res->fetch_assoc())
			{
				$relation = self::build($foreign_table);
				$relation->fill($row);
				return $relation;
			}
		}

		/**
		 * Delete the model
		 */
		public function delete()
		{
			// Request
			$sql = sprintf(self::DELETE_ROW, $this->table_name, $this->id);

			// SQL request
			try
			{
				$result = self::$executor->query($sql);
				if(!$result)
					return false;
				return true;
			}
			catch (\mysqli_sql_exception $e)
			{
				return false;
			}
		}

		public function __call(string $name, array $arguments)
		{
			// TODO corriger
			if(str_starts_with($name, 'find'))
				return AutoModel::__callStatic($name, $arguments);

			// Method name check
			if(!str_starts_with($name, 'get'))
				trigger_error('Call to undefined method  Jca\Automodel\AutoModel::' . $name, E_USER_ERROR);

			// Table name
			$array_name = NameUtils::classNameToTableName(explode('get', $name)[1]);
			$this->$array_name = [];
			if(!property_exists($this, $array_name))
				trigger_error('Call to undefined method  Jca\Automodel\AutoModel::' . $name, E_USER_ERROR);
			
			$table_name = substr($array_name, 0, -1);

			$cache_table_name = explode('_', $table_name)[0];
			$external_tables = self::getCache($cache_table_name)->getExternalTables();
			foreach($external_tables as $table => $columns)
			{
				$rows = self::$executor->getRows(['*'], $table_name, ' where ' . $columns['internal_column'] . ' = ' . $this->id);
				foreach($rows as $row)
				{
 					$model = new AutoModel($table_name);
					$model->fill($row);
					$this->$array_name[] = $model;
				}

				return $this->$array_name;
			}
		}

		/**
		 * Automatic sql request
		 * @param  string $name       Method name
		 * @param  array  $arguments  Method arguments
		 * @return string             Sql request string
		 */
		public static function __callStatic(string $name, array $arguments)
		{
			// Method name check
			if(!str_starts_with($name, 'find'))
				trigger_error('Call to undefined method  Jca\Automodel\AutoModel::' . $name, E_USER_ERROR);

			$processor = new MethodProcessor($name, $arguments);
			$processor->setCallingClass(get_called_class());

			$processor->process();
			$result = $processor->getBuilder()->asSQL();

			try
			{
				$res = self::$executor->execute_query($result['sql'], $result['values']);

				// Result
				$models = [];
				while($row = $res->fetch_assoc()){
					$model = self::build($processor->getBuilder()->getTable());
					$model->fill($row);
					$models[] = $model;
				}
				return $models;
			}
			catch (\mysqli_sql_exception $e)
			{
				return false;
			}
		}
	}
?>
