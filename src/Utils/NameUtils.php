<?php
    namespace Jca\Automodel\Utils;
        

    class NameUtils
    {
        private const DEFAULT_PATH = 'App\\Models\\';

        public static function tableNameToClassPath($table_name)
        {
            return NameUtils::DEFAULT_PATH . self::tableNameToClassName($table_name);
        }

        public static function tableNameToClassName($table_name)
        {
            $explode = explode('_', $table_name);
            return implode(array_map(function($item)
            {
                return ucfirst($item);
            }, $explode));
        }

        public static function classNameToTableName($class_name)
        {
            $split = array_filter(preg_split('/(?=[A-Z])/', $class_name), function($item)
            {
                return !empty($item);
            });

            return implode('_', array_map(function($item)
            {
                return lcfirst($item);
            }, $split));
        }

        public static function classNameToEntityName($class_name)
        {
            $split = array_filter(preg_split('/(?=[A-Z])/', $class_name), function($item)
            {
                return !empty($item);
            });

            return implode('-', array_map(function($item)
            {
                return lcfirst($item);
            }, $split));
        }
    }
?>