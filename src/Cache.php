<?php
    namespace Jca\Automodel;

    use Jca\Automodel\AutoModel;
    class Cache
    {
        private $columns = [];

        private $external_tables = [];

        public function getColumns()
        {
            return $this->columns;
        }

        public function getExternalTables()
        {
            return $this->external_tables;
        }

        public function __construct(public string $table_name)
        {
            $this->table_name = $table_name;

            $columns = AutoModel::getExecutor()->describeTable($table_name);

			foreach($columns as $column)
			{
				// Column data
				$field = $column['Field'];
				$this->columns[$field] = $column;
			
				// Foreign key
				if($column['Key'] == 'MUL')
				{
					$foreign_table = AutoModel::getExecutor()->getForeignTable($field, $table_name);
					$this->columns[$field]['REFERENCED_TABLE_NAME'] = $foreign_table['REFERENCED_TABLE_NAME'];

					// Default value
					if(!empty($column['Default']))
					{
						$methodName = 'find' . ucfirst($foreign_table['REFERENCED_TABLE_NAME']) . 'ById';
						$relation = self::$methodName($column['Default'])[0];
						$this->columns[$field]['relation'] = $relation;
					}
				}

				// External tables
				$external_tables = AutoModel::getExecutor()->getExternalTables($table_name, $field);
				foreach($external_tables as $external_table)
				{
					$external_cols = AutoModel::getExecutor()->describeTable($external_table['TABLE_NAME']);
					
					$int = current(array_filter($external_cols, fn($item) => $item['Field'] == $external_table['COLUMN_NAME'] && $item['Key'] == 'MUL'));
					$ext = current(array_filter($external_cols, fn($item) => $item['Field'] != $external_table['COLUMN_NAME'] && $item['Key'] == 'MUL'));
					
					if( $int && $ext )
                        $this->external_tables[$external_table['TABLE_NAME']] = ['internal_column' => $int['Field'], 'external_column' => $ext['Field']];
				}
			}
        }
    }
?>