<?php
	namespace Jca\Automodel\Validation;

    use Jca\Automodel\Validation\Validation;

	/**
     * Access attribute
     */
    #[\Attribute(\Attribute::TARGET_PROPERTY)]
	class Captcha extends Validation
	{
        public function __construct(public string $field) {}

        public function getValue()
        {
            return true;
        }

        public function isValid($value): bool
        {
            if( !array_key_exists($this->field . '_code', $_SESSION))
                return false;

            if( strtolower($_SESSION[$this->field . '_code']) != strtolower($value) )            
                return false;

            return true;
        }

        public function getErrorMessage()
        {
            return "Captcha incorrect";
        }
    }
?>
