<?php
	namespace Jca\Automodel\Validation;

	/**
     * Access attribute
     */
    #[\Attribute(\Attribute::TARGET_PROPERTY)]
	abstract class Validation
	{
        abstract public function getValue();
        abstract public function isValid($value) : bool;
        abstract public function getErrorMessage();
    }
?>
