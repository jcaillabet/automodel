<?php
	namespace Jca\Automodel\Validation;

    use Jca\Automodel\Validation\Validation;

	/**
     * Access attribute
     */
    #[\Attribute(\Attribute::TARGET_PROPERTY)]
	class Required extends Validation
	{
        public function getValue()
        {
            return true;
        }

        public function isValid($value) : bool
        {
            return !empty($value);
        }

        public function getErrorMessage()
        {
            return "Champ requis";
        }
    }
?>
